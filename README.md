This is a demonstration of the Atlassian-plugins web resources page data API. This API provides an ability for plugins
to inject JSON data into a page, that can be consumed by javascript.

It is an accompaniment to the documentation on developer.atlassian.com,
[Adding data providers to your plugin](https://developer.atlassian.com/display/DOCS/Adding+data+providers+to+your+plugin).

See:

- `SimpleDemoDataProvider` for an example of a simple backend implementation.
- `GsonDemoDataProvider` for a more complex backend implementation.
- `page-data-demo.js` for examples of retrieving this data via JavaScript.
