package com.atlassian.plugins.jira.page.data.demo;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

public class SimpleDemoDataProvider implements WebResourceDataProvider
{
    @Override
    public Jsonable get()
    {
        return new JsonableString("Hello world!");
    }
}